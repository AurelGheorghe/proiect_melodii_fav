const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const cors = require('cors')

const sequelize = new Sequelize('music_app','root','info20schema14',{
	dialect : 'mysql',
	define : {
		timestamps : false
	}
})

const Playlist = sequelize.define('playlist', {
    id          : {
					type: Sequelize.INTEGER,
					autoIncrement: true,
					primaryKey: true
			    	},
	title       : Sequelize.STRING,
	description :  Sequelize.TEXT
    }, {
	underscored : true
})

const Song = sequelize.define('song', {
	id      : {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true
	    	},
	artist  : Sequelize.STRING,
    title   : Sequelize.STRING
}, {
	underscored : true    
})

Playlist.hasMany(Song)

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(express.static('../redux-app/build'))

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/playlists', async (req, res) => {
	try{
		let playlists = await Playlist.findAll()
		res.status(200).json(playlists)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})


app.post('/playlists', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await Playlist.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await Playlist.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/playlists/:id', async (req, res) => {
	try{
		let playlist = await Playlist.findById(req.params.id)
		if (playlist){
			res.status(200).json(playlist)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/playlists/:id', async (req, res) => {
	try{
		let playlist = await Playlist.findById(req.params.id)
		if (playlist){
			await playlist.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/playlists/:id', async (req, res) => {
	try{
		let playlist = await Playlist.findById(req.params.id)
		if (playlist){
			await playlist.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/playlists/:pid/songs', async (req, res) => {
	try{
		let playlist = await Playlist.findById(req.params.pid)
		if (playlist){
			let songs = await playlist.getSongs()
			res.status(200).json(songs)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/playlists/:pid/songs/:sid', async (req, res) => {
	try{
		let playlist = await Playlist.findById(req.params.pid)
		if (playlist){
			let songs = await playlist.getSongs({where : {id : req.params.sid}})
			res.status(200).json(songs.shift())
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/playlists/:pid/songs', async (req, res) => {
	try{
		let playlist = await Playlist.findById(req.params.pid)
		if (playlist){
			let song = req.body
			song.playlist_id = playlist.id
			await Song.create(song)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/playlists/:pid/songs/:sid', async (req, res) => {
	try{
		let playlist = await Playlist.findById(req.params.pid)
		if (playlist){
			let songs = await playlist.getSongs({where : {id : req.params.sid}})
			let song = songs.shift()
			if (song){
				await song.update(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/playlists/:pid/songs/:sid', async (req, res) => {
	try{
		let playlist = await Playlist.findById(req.params.pid)
		if (playlist){
			let songs = await playlist.getSongs({where : {id : req.params.sid}})
			let song = songs.shift()
			if (song){
				await song.destroy(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.listen(8080)