const INITIAL_STATE = {
    playlistList : [],
    error : null,
    fetching : false,
    fetched : false
}
  
export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type){
        case 'GET_PLAYLISTS_PENDING':
        case 'ADD_PLAYLIST_PENDING':
        case 'UPDATE_PLAYLIST_PENDING':
        case 'DELETE_PLAYLIST_PENDING':
            return  {...state, error : null, fetching : true, fetched : false}           
        case 'GET_PLAYLISTS_FULFILLED':
        case 'ADD_PLAYLIST_FULFILLED':
        case 'UPDATE_PLAYLIST_FULFILLED':
        case 'DELETE_PLAYLIST_FULFILLED':                
            return {...state, playlistList : action.payload, error: null, fetched : true, fetching : false}
        case 'GET_PLAYLISTS_REJECTED':
        case 'ADD_PLAYLIST_REJECTED':
        case 'UPDATE_PLAYLIST_REJECTED':
        case 'DELETE_PLAYLIST_REJECTED':                                  
            return {...state, error : action.payload, fetching: false, fetched : false}   
        default:
            break
    }
    return state
}
