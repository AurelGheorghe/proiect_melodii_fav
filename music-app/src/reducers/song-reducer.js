const INITIAL_STATE = {
    songList : [],
    error : null,
    fetching : false,
    fetched : false
}
  
export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type){
        case 'GET_SONGS_PENDING':
        case 'ADD_SONG_PENDING':
        case 'UPDATE_SONG_PENDING':
        case 'DELETE_SONG_PENDING':
            return  {...state, error : null, fetching : true, fetched : false}           
        case 'GET_SONGS_FULFILLED':
        case 'ADD_SONG_FULFILLED':
        case 'UPDATE_SONG_FULFILLED':
        case 'DELETE_SONG_FULFILLED':                
            return {...state, songList : action.payload, error: null, fetched : true, fetching : false}
        case 'GET_SONGS_REJECTED':
        case 'ADD_SONG_REJECTED':
        case 'UPDATE_SONG_REJECTED':
        case 'DELETE_SONG_REJECTED':                                  
            return {...state, error : action.payload, fetching: false, fetched : false}
        default:
            break
    }
    return state
}
