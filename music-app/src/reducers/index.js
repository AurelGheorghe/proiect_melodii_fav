import { combineReducers } from 'redux'
import playlist from './playlist-reducer'
import song from './song-reducer'

export default combineReducers({
    playlist,
    song
})
