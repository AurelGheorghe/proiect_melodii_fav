import React, {Component} from 'react';
import PlaylistEditor from './PlaylistEditor'

class App extends Component{
  render(){
    return <PlaylistEditor /> 
  }
}

export default App;
