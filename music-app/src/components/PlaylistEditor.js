import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {Button} from 'primereact/button'
import {InputText} from 'primereact/inputtext'
import {Dialog} from 'primereact/dialog'

import {playlistActions} from '../actions'
import SongEditor from './SongEditor';

const mapStateToProps = function(state) {
    return {
        playlistList : state.playlist.playlistList,
        loading : state.playlist.fetching
    }
}

const mapDispatchToProps = function(dispatch) {
    return {
      actions: bindActionCreators({
          getPlaylists : playlistActions.getPlaylists,
          addPlaylist : playlistActions.addPlaylist,
          updatePlaylist : playlistActions.updatePlaylist,
          deletePlaylist : playlistActions.deletePlaylist
      }, dispatch)
    }
}

class PlaylistEditor extends Component{
    constructor(props){
        super(props)

        this.state = {
            playlists : [],
            showSongsFor : -1,
            isAddDialogShown : false,
            isNewPlaylist : true,
            playlist : {
                title : '',
                description : ''
            }
        }

        this.selectPlaylist = (rowData) => {
            let selected = this.state.playlists.find((e) => e.id === rowData.id)
            this.setState({
                showSongsFor: rowData.id,
                playlist : selected
            })
        }

        this.deletePlaylist = (rowData) => {
            this.props.actions.deletePlaylist(rowData.id)
        }

        this.addNewPlaylist = () => {
            let emptyPlaylist = {
                title : '',
                description : ''
            }
            this.setState({
                playlists : [],
                isAddDialogShown : true,
                isNewPlaylist : true,
                playlist : emptyPlaylist
            })
        }

        this.hideAddDialog = () => {
            this.setState({
                isAddDialogShown : false
            })
        }

        this.editPlaylist = (rowData) => {
            let playlistCopy = Object.assign({}, rowData)
            this.setState({
                playlist: playlistCopy,
                isNewPlaylist : false,
                isAddDialogShown : true
            })
        }

        this.updateProperty = (property, value) => {
            let playlist = this.state.playlist
            playlist[property] = value
            this.setState({
                playlist : playlist
            })
        }

        this.opsTemplate = (rowData) => {
            return <>
                <Button icon="pi pi-pencil" className="p-button-warning" onClick={() => this.editPlaylist(rowData)} />
                <Button icon="pi pi-times" className="p-button-danger" onClick={() => this.deletePlaylist(rowData)}  />
                <Button icon="pi pi-list" className="p-button-warning" onClick={() => this.selectPlaylist(rowData)} />
            </>
        }

        this.savePlaylist = () => {
            if (this.state.isNewPlaylist){
                this.props.actions.addPlaylist(this.state.playlist)
            }
            else{
                this.props.actions.updatePlaylist(this.state.playlist.id, this.state.playlist)
            }
            this.setState({
                isAddDialogShown : false,
                playlist: {
                    title : '',
                    description : '',
                }
            })
        }

        this.tableFooter = <div>
            <span>
                <Button label="Add" onClick={this.addNewPlaylist} icon="pi pi-plus" />
            </span>
        </div>

        this.addDialogFooter = <div>
            <Button   label="Save" icon="pi pi-save" onClick={() => this.savePlaylist()} />
        </div>
    }
    componentDidMount(){
        this.props.actions.getPlaylists()
    }
    render(){
        let {playlistList} = this.props
        return <>
            <DataTable value={playlistList} footer={this.tableFooter} >
                <Column header="Title" field="title" />
                <Column header="Description" field="description" />
                <Column body={this.opsTemplate} />
            </DataTable>
            {
                this.state.isAddDialogShown ?
                <Dialog visible={this.state.isAddDialogShown} header="Create a new playlist" onHide={this.hideAddDialog} footer={this.addDialogFooter} >
                     <InputText onChange={(e) => this.updateProperty('title', e.target.value)} value={this.state.playlist.title} name="title" placeholder="title" />
                    <InputText onChange={(e) => this.updateProperty('description', e.target.value)} value={this.state.playlist.description} name="description" placeholder="description" />
                </Dialog> 
                :
                null
            }
        </>
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistEditor)