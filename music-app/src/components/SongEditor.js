import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {Button} from 'primereact/button'
import {InputText} from 'primereact/inputtext'
import {Dialog} from 'primereact/dialog'

import {songActions} from '../actions'

const mapStateToProps = function(state) {
    return {
        songList : state.song.songList,
        loading : state.song.fetching
    }
}

const mapDispatchToProps = function(dispatch) {
    return {
      actions: bindActionCreators({
          getSongs : songActions.getSongs,
          addSong : songActions.addSong,
          updateSong : songActions.updateSong,
          deleteSong : songActions.deleteSong
      }, dispatch)
    }
}

class SongEditor extends Component{
    constructor(props){
        super(props)

        this.state = {
            isAddDialogShown : false,
            isNewSong : true,
            song : {
                artist : '',
                title : ''
            }
        }

        this.deleteSong = (rowData) => {
            this.props.actions.deleteSong(rowData.id)
        }

        this.addNewSong = () => {
            let emptySong = {
                artist : '',
                title : ''
            }
            this.setState({
                isAddDialogShown : true,
                isNewSong : true,
                song : emptySong
            })
        }

        this.hideAddDialog = () => {
            this.setState({
                isAddDialogShown : false
            })
        }

        this.editSong = (rowData) => {
            let songCopy = Object.assign({}, rowData)
            this.setState({
                song: songCopy,
                isNewSong : false,
                isAddDialogShown : true
            })
        }

        this.updateProperty = (property, value) => {
            let song = this.state.song
            song[property] = value
            this.setState({
                song : song
            })
        }

        this.opsTemplate = (rowData) => {
            return <>
                <Button icon="pi pi-times" className="p-button-danger" onClick={() => this.deleteSong(rowData)}  />
                <Button icon="pi pi-pencil" className="p-button-warning" onClick={() => this.editSong(rowData)} />
            </>
        }

        this.saveSong = () => {
            if (this.state.isNewSong){
                this.props.actions.addSong(this.state.song)
            }
            else{
                this.props.actions.updateSong(this.state.song.id, this.state.song)
            }
            this.setState({
                isAddDialogShown : false,
                song: {
                    artist : '',
                    title : '',
                }
            })
        }

        this.tableFooter = <div>
            <span>
                <Button label="Add" onClick={this.addNewSong} icon="pi pi-plus" />
            </span>
        </div>

        this.addDialogFooter = <div>
            <Button   label="Save" icon="pi pi-save" onClick={() => this.saveSong()} />
        </div>
    }
    componentDidMount(){
        this.props.actions.getSongs()
    }
    render(){
        let {songList} = this.props
        return <>
            <DataTable value={songList} footer={this.tableFooter} >
                <Column header="artist" field="artist" />
                <Column header="title" field="title" />
                <Column body={this.opsTemplate} />
            </DataTable>
            {
                this.state.isAddDialogShown ?
                <Dialog visible={this.state.isAddDialogShown} header="Register a new song" onHide={this.hideAddDialog} footer={this.addDialogFooter} >
                     <InputText onChange={(e) => this.updateProperty('artist', e.target.value)} value={this.state.song.artist} name="artist" placeholder="artist" />
                    <InputText onChange={(e) => this.updateProperty('title', e.target.value)} value={this.state.song.title} name="title" placeholder="title" />
                </Dialog> 
                :
                null
            }
        </>
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SongEditor)