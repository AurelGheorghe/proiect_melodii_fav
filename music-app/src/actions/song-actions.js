import {SERVER} from '../config/global'

export const GET_SONGS = 'GET_SONGS'
export const ADD_SONG = 'ADD_SONG'
export const UPDATE_SONG = 'UPDATE_SONG'
export const DELETE_SONG = 'DELETE_SONG'

export function getSongs(playlistId) {
    return {
      type: GET_SONGS,
      payload: async () => {
          let response  = await fetch(`${SERVER}/playlists/${playlistId}/songs`)
          let json = await response.json()
          return json
      }
    }
}

export function addSong(song, playlistId){
    return {
      type : ADD_SONG,
      payload : async () => {
          await fetch(`${SERVER}/playlists/${playlistId}/songs`, {
              method : 'post',
              headers : {
                  'Content-Type' : 'application/json'
              },
              body : JSON.stringify(song)
          })
          let response  = await fetch(`${SERVER}/playlists/${playlistId}/songs`)
          let json = await response.json()
          return json
      }
    }
  }

  export function updateSong(songId, song, playlistId){
    return {
        type : UPDATE_SONG,
        payload : async () => {
            await fetch(`${SERVER}/playlists/${playlistId}/songs/${songId}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(song)
            })
            let response  = await fetch(`${SERVER}/playlists/${playlistId}/songs`)
            let json = await response.json()
            return json
        }
    }
}

export function deleteSong(songId, playlistId){
    return {
        type : DELETE_SONG,
        payload : async () => {
            await fetch(`${SERVER}/playlists/${playlistId}/songs/${songId}`, {
                method : 'delete'
            })
            let response  = await fetch(`${SERVER}/playlists/${playlistId}/songs`)
            let json = await response.json()
            return json
        }
    }
}


  