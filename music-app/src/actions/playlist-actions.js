import {SERVER} from '../config/global'

export const GET_PLAYLISTS = 'GET_PLAYLISTS'
export const ADD_PLAYLIST = 'ADD_PLAYLIST'
export const UPDATE_PLAYLIST = 'UPDATE_PLAYLIST'
export const DELETE_PLAYLIST = 'DELETE_PLAYLIST'

export function getPlaylists() {
    return {
      type: GET_PLAYLISTS,
      payload: async () => {
          let response  = await fetch(`${SERVER}/playlists`)
          let json = await response.json()
          return json
      }
    }
}

export function addPlaylist(playlist){
    return {
      type : ADD_PLAYLIST,
      payload : async () => {
          await fetch(`${SERVER}/playlists`, {
              method : 'post',
              headers : {
                  'Content-Type' : 'application/json'
              },
              body : JSON.stringify(playlist)
          })
          let response  = await fetch(`${SERVER}/playlists`)
          let json = await response.json()
          return json
      }
    }
  }

  export function updatePlaylist(playlistId, playlist){
    return {
        type : UPDATE_PLAYLIST,
        payload : async () => {
            await fetch(`${SERVER}/playlists/${playlistId}`, {
                method : 'put',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(playlist)
            })
            let response  = await fetch(`${SERVER}/playlists`)
            let json = await response.json()
            return json
        }
    }
}

export function deletePlaylist(playlistId){
    return {
        type : DELETE_PLAYLIST,
        payload : async () => {
            await fetch(`${SERVER}/playlists/${playlistId}`, {
                method : 'delete'
            })
            let response  = await fetch(`${SERVER}/playlists`)
            let json = await response.json()
            return json
        }
    }
}
